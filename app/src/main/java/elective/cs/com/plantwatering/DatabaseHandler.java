package elective.cs.com.plantwatering;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "plantwatering_db";

    // SCHEDULES TABLE
    private static final String TABLE_SCHEDULES = "schedules";
    private static final String KEY_SCHED_ID = "ID";
    private static final String KEY_SCHED_TIME = "schedule_time";
    private static final String KEY_REPEAT = "repeat_days";
    private static final String KEY_ENABLED = "enabled";
    private static final String KEY_REPEATED = "repeated";

    // HISTORY TABLE
    private static final String TABLE_HISTORY = "history";
    private static final String KEY_HIST_ID = "ID";
    private static final String KEY_HIST_DATE = "history_date";

    DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // CREATE SCHEDULES TABLE
        String CREATE_SCHEDULES_TABLE = "CREATE TABLE " + TABLE_SCHEDULES + "("
                + KEY_SCHED_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_SCHED_TIME + " TEXT, "
                + KEY_REPEAT + " TEXT, "
                + KEY_ENABLED + " INTEGER, "
                + KEY_REPEATED + " INTEGER" + ")";
        db.execSQL(CREATE_SCHEDULES_TABLE);


        // CREATE HISTORY TABLE
        String CREATE_HISTORY_TABLE = "CREATE TABLE " + TABLE_HISTORY + "("
                + KEY_HIST_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_HIST_DATE + " TEXT" + ")";
        db.execSQL(CREATE_HISTORY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULES);
        onCreate(db);
    }

    // FETCHING SCHEDULES DATA

    int[] getScheduleIDs() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + KEY_SCHED_ID + " FROM " + TABLE_SCHEDULES;
        Cursor cursor = db.rawQuery(query, null);
        int[] result = new int[cursor.getCount()];
        int i = 0;
        if (cursor.moveToFirst()) {
            do {
                result[i] = cursor.getInt(0);
                i++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    String[] getScheduleTimes() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + KEY_SCHED_TIME + " FROM " + TABLE_SCHEDULES;
        Cursor cursor = db.rawQuery(query, null);
        String[] result = new String[cursor.getCount()];
        int i = 0;
        if (cursor.moveToFirst()) {
            do {
                result[i] = cursor.getString(0);
                i++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    String[] getScheduleRepeatDays() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + KEY_REPEAT + " FROM " + TABLE_SCHEDULES;
        Cursor cursor = db.rawQuery(query, null);
        String[] result = new String[cursor.getCount()];
        int i = 0;
        if (cursor.moveToFirst()) {
            do {
                result[i] = cursor.getString(0);
                i++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    Boolean[] getScheduleStatus() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + KEY_ENABLED+ " FROM " + TABLE_SCHEDULES;
        Cursor cursor = db.rawQuery(query, null);
        Boolean[] result = new Boolean[cursor.getCount()];
        int i = 0;
        if (cursor.moveToFirst()) {
            do {
                result[i] = cursor.getInt(0) > 0;
                i++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }


    // FETCHING SELECTED SCHEDULE DATA

    String getSelectedScheduleTime(int sched_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + KEY_SCHED_TIME + " FROM " + TABLE_SCHEDULES + " WHERE " + KEY_SCHED_ID + " = " + sched_id;
        Cursor cursor = db.rawQuery(query, null);
        String result = "";
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        cursor.close();
        return result;
    }

    String getSelectedScheduleRepeatDays(int sched_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + KEY_REPEAT + " FROM " + TABLE_SCHEDULES + " WHERE " + KEY_SCHED_ID + " = " + sched_id;
        Cursor cursor = db.rawQuery(query, null);
        String result = "";
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }

        cursor.close();
        return result;
    }

    Boolean getSelectedScheduleEnableStatus(int sched_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + KEY_ENABLED + " FROM " + TABLE_SCHEDULES + " WHERE " + KEY_SCHED_ID + " = " + sched_id;
        Cursor cursor = db.rawQuery(query, null);
        Boolean result = false;
        if (cursor.moveToFirst()) {
            result = cursor.getInt(0) > 0;
        }

        cursor.close();
        return result;
    }

    Boolean getSelectedScheduleRepeatStatus(int sched_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + KEY_REPEATED + " FROM " + TABLE_SCHEDULES + " WHERE " + KEY_SCHED_ID + " = " + sched_id;
        Cursor cursor = db.rawQuery(query, null);
        Boolean result = false;
        if (cursor.moveToFirst()) {
            result = cursor.getInt(0) > 0;
        }

        cursor.close();
        return result;
    }


    // FETCHING HISTORY DATA

    int[] getHistoryIDs() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + KEY_HIST_ID + " FROM " + TABLE_HISTORY;
        Cursor cursor = db.rawQuery(query, null);
        int[] result = new int[cursor.getCount()];
        int i = 0;
        if (cursor.moveToFirst()) {
            do {
                result[i] = cursor.getInt(0);
                i++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    String[] getHistoryDates() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + KEY_HIST_DATE + " FROM " + TABLE_HISTORY;
        Cursor cursor = db.rawQuery(query, null);
        String[] result = new String[cursor.getCount()];
        int i = 0;
        if (cursor.moveToFirst()) {
            do {
                result[i] = cursor.getString(0);
                i++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }


    // INSERT NEW SCHEDULE TO DATABASE

    void newSchedule(String time) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "INSERT INTO " + TABLE_SCHEDULES + "(" + KEY_SCHED_TIME + ", " + KEY_REPEAT + ", " + KEY_ENABLED + ", " + KEY_REPEATED + ") VALUES (" + DatabaseUtils.sqlEscapeString(time) + "," +  DatabaseUtils.sqlEscapeString("") + ", " + 1 + ", " + 0 + ")";
        db.execSQL(query);
        db.close();
    }


    // UPDATE SCHEDULE STATUS (ENABLE/DISABLE)

    void setScheduleEnableStatus(int sched_id, int enabled) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_SCHEDULES + " SET " + KEY_ENABLED + " = " + enabled + " WHERE " + KEY_SCHED_ID + " = " + sched_id;
        db.execSQL(query);
        db.close();
    }

    void setScheduleRepeatStatus(int sched_id, int repeated) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_SCHEDULES + " SET " + KEY_REPEATED + " = " + repeated + " WHERE " + KEY_SCHED_ID + " = " + sched_id;
        db.execSQL(query);
        db.close();
    }

    void setRepeatDays(int sched_id, String days) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_SCHEDULES + " SET " + KEY_REPEAT + " = " + DatabaseUtils.sqlEscapeString(days) + " WHERE " + KEY_SCHED_ID + " = " + sched_id;
        db.execSQL(query);
        db.close();
    }

    void setTime(int sched_id, String time) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_SCHEDULES + " SET " + KEY_SCHED_TIME + " = " + DatabaseUtils.sqlEscapeString(time) + " WHERE " + KEY_SCHED_ID + " = " + sched_id;
        db.execSQL(query);
        db.close();
    }

    // DELETING OF DATA INSIDE THE DATABASE

    // DELETE SCHEDULE
    void removeSchedule(int sched_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_SCHEDULES + " WHERE " + KEY_SCHED_ID + " = " + sched_id;
        db.execSQL(query);
        db.close();
    }

    // DELETE SELECTED HISTORY
    void removeHistory(int hist_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_HISTORY + " WHERE " + KEY_HIST_ID + " = " + hist_id;
        db.execSQL(query);
        db.close();
    }

    // DELETE ALL DATA ON HISTORY TABLE
    void removeEntireHistory() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_HISTORY;
        db.execSQL(query);
        db.close();
    }
}
