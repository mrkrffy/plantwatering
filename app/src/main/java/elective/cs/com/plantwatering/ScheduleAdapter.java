package elective.cs.com.plantwatering;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

public class ScheduleAdapter extends BaseAdapter {

    private Context context;
    private String[] time = {}, days = {};
    private Boolean[] switch_state = {};
    private int[] sched_id;

    private ImageView icon;
    private TextView text_time, text_meridiem, text_days;
    private Switch sched_switch;

    private DatabaseHandler db;

    @Override
    public int getCount() {
        return time.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = View.inflate(context, R.layout.sched_list_view, null);

        // Instantiate Database Class
        db = new DatabaseHandler(context);

        // Views from sched_list_view.xml
        icon = view.findViewById(R.id.sched_icon);
        text_time = view.findViewById(R.id.sched_time);
        text_meridiem = view.findViewById(R.id.sched_meridiem);
        text_days = view.findViewById(R.id.sched_days);
        sched_switch = view.findViewById(R.id.sched_switch);

        // Set view values
        text_time.setText(getTime(time[i]));
        text_meridiem.setText(getMeridiem(time[i]));
        sched_switch.setChecked(switch_state[i]);
        sched_switch.setTag(sched_id[i]);

        // Set Image if AM or PM
        if (getMeridiem(time[i]).equals("AM")) {
            icon.setImageResource(R.drawable.ic_day);
        } else {
            icon.setImageResource(R.drawable.ic_night);
        }

        // Set Visibility of Repeat Days when there's nothing
        if (db.getSelectedScheduleRepeatStatus(sched_id[i])) {
            text_days.setText(days[i]);
        } else {
            text_days.setVisibility(View.GONE);
        }

        // Check if switch is enabled/true
        if (switch_state[i]) {
            text_time.setTextColor(Color.BLACK);
            text_meridiem.setTextColor(Color.BLACK);
            text_days.setTextColor(Color.BLACK);
            icon.setColorFilter(Color.BLACK);
        } else {
            text_time.setTextColor(Color.GRAY);
            text_meridiem.setTextColor(Color.GRAY);
            text_days.setTextColor(Color.GRAY);
            icon.setColorFilter(Color.GRAY);
        }

        // Getting the inflated view and setting it to final, for usage inside a listener
        final View finalView = view;
        sched_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                // Redeclare views to use per list item
                icon = finalView.findViewById(R.id.sched_icon);
                text_time = finalView.findViewById(R.id.sched_time);
                text_meridiem = finalView.findViewById(R.id.sched_meridiem);
                text_days = finalView.findViewById(R.id.sched_days);
                sched_switch = finalView.findViewById(R.id.sched_switch);

                // Check if switch is enabled
                if (b) {
                    text_time.setTextColor(Color.BLACK);
                    text_meridiem.setTextColor(Color.BLACK);
                    text_days.setTextColor(Color.BLACK);
                    icon.setColorFilter(Color.BLACK);
                } else {
                    text_time.setTextColor(Color.GRAY);
                    text_meridiem.setTextColor(Color.GRAY);
                    text_days.setTextColor(Color.GRAY);
                    icon.setColorFilter(Color.GRAY);
                }

                db.setScheduleEnableStatus(Integer.parseInt(sched_switch.getTag().toString()), b ? 1 : 0);
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sched_switch = finalView.findViewById(R.id.sched_switch);
                Intent intent = new Intent(context, SetScheduleActivity.class);
                intent.putExtra("sched_id", Integer.parseInt(sched_switch.getTag().toString()));
                context.startActivity(intent);
            }
        });

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return false;
            }
        });

        return view;
    }

    private String getTime(String time) {
        String[] temp_time = time.split(" ");
        return temp_time[0];
    }

    private String getMeridiem(String time) {
        String[] temp_time = time.split(" ");
        return temp_time[1];
    }

    void setContext(Context context) { this.context = context; }

    void setSchedules(int[] sched_id, String[] time, String[] days, Boolean[] switch_state) {
        this.sched_id = sched_id;
        this.time = time;
        this.days = days;
        this.switch_state = switch_state;
    }
}
