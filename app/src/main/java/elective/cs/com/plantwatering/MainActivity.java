package elective.cs.com.plantwatering;

import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class MainActivity extends FragmentActivity {

    BottomNavigationViewEx bottomNav;

    private static final int NUM_PAGES = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Bottom Navigation
        bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.enableShiftingMode(false);
        bottomNav.setIconSize(20, 20);
        bottomNav.setTextSize(10);

        // View Pager and Pager Adapter for fragment handling / sliding
        ViewPager viewPager = findViewById(R.id.view_pager);
        PagerAdapter pagerAdapter = new NavigationPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        // Bind view pager and bottom navigation
        bottomNav.setupWithViewPager(viewPager);
    }

    private class NavigationPagerAdapter extends FragmentStatePagerAdapter {

        private NavigationPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new HomeFragment();
                case 1:
                    return new SchedulesFragment();
                case 2:
                    return new HistoryFragment();
                case 3:
                    return new SettingsFragment();
            }
            return new HomeFragment();
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
