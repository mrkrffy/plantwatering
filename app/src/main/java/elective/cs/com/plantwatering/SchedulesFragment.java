package elective.cs.com.plantwatering;


import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.tomerrosenfeld.customanalogclockview.CustomAnalogClock;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class SchedulesFragment extends Fragment {


    public SchedulesFragment() {
        // Required empty public constructor
    }

    private DatabaseHandler db;

    private ScheduleAdapter scheduleAdapter;
    private ImageButton button_mode;
    private Calendar calendar;
    private TimePickerDialog.OnTimeSetListener time_picker;

    private LinearLayout no_sched;
    private ListView sched_list;

    private int[] sched_id = {};
    private String[] time = {}, days = {};
    private Boolean[] sched_status = {};

    private int selected_sched_id;
    private Boolean selected_sched_enabled;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_schedules, container, false);

        // Instantiate Database Handler and Get Schedules Data
        db = new DatabaseHandler(getActivity());
        getSchedules();

        // Analog Clock
        CustomAnalogClock analogClock = view.findViewById(R.id.analog_clock);
        analogClock.setScale(0.8f);
        analogClock.setAutoUpdate(true);
        analogClock.init(getActivity(), R.drawable.analog_face, R.drawable.minute_hand, R.drawable.hour_hand, 0, false, false);

        // Schedule List
        sched_list = view.findViewById(R.id.sched_list);
        scheduleAdapter = new ScheduleAdapter();
        scheduleAdapter.setContext(getActivity());
        scheduleAdapter.setSchedules(sched_id, time, days, sched_status);
        sched_list.setAdapter(scheduleAdapter);
        registerForContextMenu(sched_list);

        // No Schedules
        no_sched = view.findViewById(R.id.no_sched);
        if (sched_id == null || sched_id.length <= 0) {
            no_sched.setVisibility(View.VISIBLE);
            sched_list.setVisibility(View.GONE);
        }

        // Add/New Schedule Button
        ImageButton button_add = view.findViewById(R.id.button_add_sched);
        calendar = Calendar.getInstance();
        time_picker = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                newSchedule();
                updateScheduleList();
            }
        };
        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(getActivity(),
                        time_picker,
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        false)
                        .show();
            }
        });

        // Change Mode Button
        button_mode = view.findViewById(R.id.button_mode);
        button_mode.setTag(R.drawable.ic_mode_manual);
        button_mode.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch(motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        button_mode.animate()
                                .setDuration(50)
                                .scaleX(0.90f)
                                .scaleY(0.90f);
                        break;
                    case MotionEvent.ACTION_UP:
                        button_mode.animate()
                                .setDuration(50)
                                .scaleX(1.0f)
                                .scaleY(1.0f)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        if((int)button_mode.getTag() == R.drawable.ic_mode_manual) {
                                            button_mode.setImageResource(R.drawable.ic_mode_automatic);
                                            button_mode.setTag(R.drawable.ic_mode_automatic);
                                        } else {
                                            button_mode.setImageResource(R.drawable.ic_mode_manual);
                                            button_mode.setTag(R.drawable.ic_mode_manual);
                                        }

                                        Toast.makeText(getActivity(), R.string.toast_mode_changed, Toast.LENGTH_SHORT).show();
                                    }
                                });
                        break;
                }
                return true;
            }
        });

        sched_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                selected_sched_id = sched_id[i];
                selected_sched_enabled = sched_status[i];
                return false;
            }
        });

        return view;
    }

    private void updateScheduleList() {
        getSchedules();

        scheduleAdapter.setSchedules(sched_id, time, days, sched_status);
        scheduleAdapter.notifyDataSetChanged();

        if (sched_id == null || sched_id.length <= 0) {
            no_sched.setVisibility(View.VISIBLE);
            sched_list.setVisibility(View.GONE);
        } else {
            no_sched.setVisibility(View.GONE);
            sched_list.setVisibility(View.VISIBLE);
        }
    }

    private void getSchedules() {
        sched_id = db.getScheduleIDs();
        time = db.getScheduleTimes();
        days = db.getScheduleRepeatDays();
        sched_status = db.getScheduleStatus();
    }

    private void newSchedule() {
        String myFormat = "hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        if (calendar == null) { calendar = Calendar.getInstance(); }
        db.newSchedule(sdf.format(calendar.getTime()));
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle("What do you want to do?");

        if (!selected_sched_enabled) {
            menu.add(0, v.getId(), 0, "Turn On Schedule");
        } else {
            menu.add(0, v.getId(), 0, "Turn Off Schedule");
        }
        menu.add(0, v.getId(), 0, "Change Schedule");
        menu.add(0, v.getId(), 0, "Delete");
        menu.add(0, v.getId(), 0, "Close");

        for (int i=0; i < menu.size(); i++){
            menu.getItem(i).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getTitle().toString().toLowerCase()) {
                        case "turn off schedule":
                            db.setScheduleEnableStatus(selected_sched_id, 0);
                            updateScheduleList();
                            break;
                        case "turn on schedule":
                            db.setScheduleEnableStatus(selected_sched_id, 1);
                            updateScheduleList();
                            break;
                        case "change schedule":
                            Intent intent = new Intent(getActivity(), SetScheduleActivity.class);
                            intent.putExtra("sched_id", selected_sched_id);
                            startActivity(intent);
                            break;
                        case "delete":
                            new AlertDialog.Builder(getActivity())
                                    .setMessage("Are you sure?")
                                    .setNegativeButton("No", null)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            db.removeSchedule(selected_sched_id);
                                            updateScheduleList();
                                            Toast.makeText(getActivity(), R.string.toast_deleted, Toast.LENGTH_SHORT).show();
                                        }
                                    })
                                    .create()
                                    .show();
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateScheduleList();
    }

}
