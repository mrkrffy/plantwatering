package elective.cs.com.plantwatering;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {


    public HistoryFragment() {
        // Required empty public constructor
    }

    private EditText spec_date;
    private Calendar calendar;
    private ImageButton button_filter, button_delete_all;
    private DatePickerDialog.OnDateSetListener date_picker;

    private String[] time = {"10:00", "05:00", "07:00", "12:00"},
            days = {"Today", "Yesterday", "Mon", "Jan 10"},
            meridiem = {"AM", "PM", "AM", "AM"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        // History List
        ListView history_list = view.findViewById(R.id.history_list);
        HistoryAdapter historyAdapter = new HistoryAdapter();
        historyAdapter.setContext(getActivity());
        historyAdapter.setSchedule(time, days, meridiem);
        history_list.setAdapter(historyAdapter);

        // Date Picker
        spec_date = view.findViewById(R.id.edittext_spec);
        calendar = Calendar.getInstance();
        date_picker = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDateEditText();
            }

        };
        spec_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(),
                        date_picker,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH))
                        .show();
            }
        });

        // Filter Button
        button_filter = view.findViewById(R.id.button_filter);
        button_filter.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch(motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        button_filter.animate()
                                .setDuration(50)
                                .scaleX(0.90f)
                                .scaleY(0.90f);
                        break;
                    case MotionEvent.ACTION_UP:
                        button_filter.animate()
                                .setDuration(50)
                                .scaleX(1.0f)
                                .scaleY(1.0f)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), R.string.toast_coming_soon, Toast.LENGTH_SHORT).show();
                                    }
                                });
                        break;
                }
                return true;
            }
        });

        // Delete All History Button
        button_delete_all = view.findViewById(R.id.button_delete_all);
        button_delete_all.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch(motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        button_delete_all.animate()
                                .setDuration(50)
                                .scaleX(0.90f)
                                .scaleY(0.90f);
                        break;
                    case MotionEvent.ACTION_UP:
                        button_delete_all.animate()
                                .setDuration(50)
                                .scaleX(1.0f)
                                .scaleY(1.0f)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        new AlertDialog.Builder(getActivity())
                                                .setMessage("Are you sure you want to delete the entire irrigation history?")
                                                .setNegativeButton("No", null)
                                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        Toast.makeText(getActivity(), R.string.toast_coming_soon, Toast.LENGTH_SHORT).show();
                                                    }
                                                })
                                                .create()
                                                .show();
                                    }
                                });
                        break;
                }
                return true;
            }
        });

        return view;
    }

    private void updateDateEditText() {
        String myFormat = "EEE, MMM dd, yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        spec_date.setText(sdf.format(calendar.getTime()));
    }

}
