package elective.cs.com.plantwatering;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import info.abdolahi.CircularMusicProgressBar;
import info.abdolahi.OnCircularSeekBarChangeListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    public HomeFragment() {
        // Required empty public constructor
    }

    private CircularMusicProgressBar button_irrigate;
    private LinearLayout button_mode;
    private ImageButton button_camera;
    private TextView mode_main_text;
    private Boolean full = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        // Irrigation Overview
        setOverviewValues(view);

        // Irrigate Button
        button_irrigate = view.findViewById(R.id.button_irrigate);
        button_irrigate.setValue(100);
        button_irrigate.setProgressAnimationState(true);
        button_irrigate.setOnCircularBarChangeListener(new OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularMusicProgressBar circularBar, int progress, boolean fromUser) {

            }

            @Override
            public void onClick(CircularMusicProgressBar circularBar) {
                new AlertDialog.Builder(getActivity())
                        .setMessage("Water your plants now?")
                        .setNegativeButton("Cancel", null)
                        .setPositiveButton("Water Now", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (full) {
                                    button_irrigate.setValue(0);
                                    full = false;
                                } else {
                                    button_irrigate.setValue(100);
                                    full = true;
                                }
                                Toast.makeText(getActivity(), R.string.toast_watering, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .create()
                        .show();
            }

            @Override
            public void onLongPress(CircularMusicProgressBar circularBar) {

            }
        });

        // Change Mode Button
        button_mode = view.findViewById(R.id.button_mode);
        mode_main_text = view.findViewById(R.id.text_mode);
        button_mode.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        button_mode.animate()
                                .setDuration(50)
                                .scaleX(0.90f)
                                .scaleY(0.90f);
                        break;
                    case MotionEvent.ACTION_UP:
                        button_mode.animate()
                                .setDuration(50)
                                .scaleX(1.0f)
                                .scaleY(1.0f)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mode_main_text.getText().toString().equals("Automatic Mode")) {
                                            mode_main_text.setText(R.string.mode_manual);
                                        } else {
                                            mode_main_text.setText(R.string.mode_auto);
                                        }
                                    }
                                });
                        break;
                }
                return true;
            }
        });

        // Camera Button
        button_camera = view.findViewById(R.id.button_camera);
        button_camera.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        button_camera.animate()
                                .setDuration(50)
                                .scaleX(0.90f)
                                .scaleY(0.90f);
                        break;
                    case MotionEvent.ACTION_UP:
                        button_camera.animate()
                                .setDuration(50)
                                .scaleX(1.0f)
                                .scaleY(1.0f)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), R.string.toast_coming_soon, Toast.LENGTH_SHORT).show();
                                    }
                                });
                        break;
                }
                return true;
            }
        });

        return view;
    }

    private void setOverviewValues(View view) {
        String recent_time = "05:00",
               recent_meridiem = "PM",
               recent_day = "Today",
               next_time = "07:00",
               next_meridiem = "PM",
               next_day = "Today";

        // Recent Irrigation Time TextViews
        TextView recent_time_text = view.findViewById(R.id.recent_time);
        TextView recent_meridiem_text = view.findViewById(R.id.recent_meridiem);
        TextView recent_day_text = view.findViewById(R.id.recent_day);

        // Next Irrigation Time TextViews
        TextView next_time_text = view.findViewById(R.id.next_time);
        TextView next_meridiem_text = view.findViewById(R.id.next_meridiem);
        TextView next_day_text = view.findViewById(R.id.next_day);

        // Set Text For Recent Irrigation Text Views
        recent_time_text.setText(recent_time);
        recent_meridiem_text.setText(recent_meridiem);
        recent_day_text.setText(recent_day);

        // Set Text For Next Irrigation Text Views
        next_time_text.setText(next_time);
        next_meridiem_text.setText(next_meridiem);
        next_day_text.setText(next_day);
    }

}
