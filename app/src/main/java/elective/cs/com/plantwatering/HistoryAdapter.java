package elective.cs.com.plantwatering;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class HistoryAdapter extends BaseAdapter {

    private Context context;
    private String[] time = {}, days = {}, meridiem = {};

    @Override
    public int getCount() {
        return time.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = View.inflate(context, R.layout.history_list_view, null);

        // Views from history_list_view.xml
        ImageView icon = view.findViewById(R.id.sched_icon);
        TextView text_time = view.findViewById(R.id.sched_time);
        TextView text_meridiem = view.findViewById(R.id.sched_meridiem);
        TextView text_day = view.findViewById(R.id.sched_days);
        ImageView delete = view.findViewById(R.id.hist_delete);

        // Set view values
        view.setClickable(false);
        icon.setImageResource(R.drawable.ic_day);
        text_time.setText(time[i]);
        text_meridiem.setText(meridiem[i]);
        text_day.setText(days[i]);
        delete.setTag(i);

        // Set per list item delete onclicklistener
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(context)
                        .setMessage("Are you sure?")
                        .setNegativeButton("No", null)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(context, R.string.toast_coming_soon, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .create()
                        .show();
            }
        });

        // Return the view
        return view;
    }



    void setContext(Context context) { this.context = context; }

    void setSchedule(String[] time, String[] days, String[] meridiem) {
        this.time = time;
        this.days = days;
        this.meridiem = meridiem;
    }
}
