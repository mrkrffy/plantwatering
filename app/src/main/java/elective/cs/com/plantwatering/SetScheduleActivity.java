package elective.cs.com.plantwatering;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SetScheduleActivity extends AppCompatActivity {

    private LinearLayout repeat_selection;
    private Calendar calendar;
    private TimePickerDialog.OnTimeSetListener time_picker;
    private TextView time_text_value, repeat_text_view;
    private RadioButton everyday, weekdays, weekend, weekly;
    private CheckBox weekly_mon, weekly_tue, weekly_wed, weekly_thu, weekly_fri, weekly_sat, weekly_sun;
    private TableLayout weekly_days_container;


    private DatabaseHandler db;

    private int sched_id;

    private Boolean turned_on = false, repeated = false;
    private String sched_time = "";
    private String selected_repeat_text = "";
    private String mon = "", tue = "", wed = "", thu = "", fri = "", sat = "", sun = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_schedule);

        // Get data and load schedule
        Bundle extras = getIntent().getExtras();
        sched_id = extras.getInt("sched_id");

        // Instantiate Database Handler and Get Schedules Data
        db = new DatabaseHandler(this);
        getSelectedSchedule();
        updateWeeklyDays();

        // Toolbar/Actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Turn On Schedule Checkbox
        CheckBox check_sched_on = findViewById(R.id.sched_check);
        check_sched_on.setChecked(turned_on);
        check_sched_on.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                db.setScheduleEnableStatus(sched_id, checked ? 1 : 0);
            }
        });

        // Change Time Item
        LinearLayout change_time = findViewById(R.id.sched_time);
        calendar = Calendar.getInstance();
        time_picker = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                updateTimeText();
            }
        };
        change_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSelectedSchedule();
                new TimePickerDialog(SetScheduleActivity.this,
                        time_picker,
                        Integer.parseInt(sched_time.split(" ")[0].split(":")[0]),
                        Integer.parseInt(sched_time.split(" ")[0].split(":")[1]),
                        false)
                        .show();
            }
        });

        // Current Time
        time_text_value = findViewById(R.id.time_sub_text);
        time_text_value.setText(sched_time);

        // Current Repeat Days Text View
        repeat_text_view = findViewById(R.id.repeat_text_days);

        // Repeat Switch
        Switch repeat = findViewById(R.id.sched_switch);
        repeat.setChecked(repeated);
        repeat_selection = findViewById(R.id.repeat_container);
        if (repeated) {
            repeat_selection.setVisibility(View.VISIBLE);
        }
        repeat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                repeat_selection.setVisibility(checked ? View.VISIBLE : View.GONE);
                db.setScheduleRepeatStatus(sched_id, checked ? 1 : 0);
                getSelectedSchedule();
                getSelectedRepeatOption();
            }
        });

        // Repeat Radio Button Options
        everyday = findViewById(R.id.repeat_radio_everyday);
        weekdays = findViewById(R.id.repeat_radio_weekdays);
        weekend = findViewById(R.id.repeat_radio_weekends);
        weekly = findViewById(R.id.repeat_radio_weekly);
        weekly_days_container = findViewById(R.id.weekly_days);

        if (repeated) {
            String[] temp_repeat = db.getSelectedScheduleRepeatDays(sched_id).split(",");
            switch (temp_repeat[0]) {
                case "Everyday":
                    setCheckedRepeatOption(everyday);
                    break;
                case "Weekdays":
                    setCheckedRepeatOption(weekdays);
                    break;
                case "Weekends":
                    setCheckedRepeatOption(weekend);
                    break;
                default:
                    setCheckedRepeatOption(weekly);
                    weekly_days_container.setVisibility(View.VISIBLE);
            }
        }

        // Repeat Radio Buttons Listeners
        everyday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    setCheckedRepeatOption(everyday);
                    selected_repeat_text = "Everyday";
                    setRepeatDaysValue();
                }
            }
        });
        weekdays.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    setCheckedRepeatOption(weekdays);
                    selected_repeat_text = "Weekdays";
                    setRepeatDaysValue();
                }
            }
        });
        weekend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    setCheckedRepeatOption(weekend);
                    selected_repeat_text = "Weekends";
                    setRepeatDaysValue();
                }
            }
        });
        weekly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    setCheckedRepeatOption(weekly);
                    weekly_days_container.setVisibility(View.VISIBLE);
                    updateWeeklyDays();
                    if (db.getSelectedScheduleRepeatDays(sched_id).equals("Everyday") || db.getSelectedScheduleRepeatDays(sched_id).equals("Weekdays") || db.getSelectedScheduleRepeatDays(sched_id).equals("Weekends")) {
                        repeat_text_view.setText("");
                    } else {
                        getSelectedRepeatOption();
                        updateWeeklyDays();
                        setRepeatDaysValue();
                    }
                }
            }
        });

        if (!repeated) {
            everyday.setChecked(true);
        }

        // Weekly Days Checkboxes
        weekly_mon = findViewById(R.id.weekly_mon);
        weekly_tue = findViewById(R.id.weekly_tue);
        weekly_wed = findViewById(R.id.weekly_wed);
        weekly_thu = findViewById(R.id.weekly_thu);
        weekly_fri = findViewById(R.id.weekly_fri);
        weekly_sat = findViewById(R.id.weekly_sat);
        weekly_sun = findViewById(R.id.weekly_sun);

        // Repeat Days
        getSelectedRepeatOption();

        // Weekly Days Checkbox Listeners
        weekly_mon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mon = ", Mon";
                } else {
                    mon = "";
                }
                updateWeeklyDays();
                setRepeatDaysValue();
            }
        });
        weekly_tue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    tue = ", Tue";
                } else {
                    tue = "";
                }
                updateWeeklyDays();
                setRepeatDaysValue();
            }
        });
        weekly_wed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    wed = ", Wed";
                } else {
                    wed = "";
                }
                updateWeeklyDays();
                setRepeatDaysValue();
            }
        });
        weekly_thu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    thu = ", Thu";
                } else {
                    thu = "";
                }
                updateWeeklyDays();
                setRepeatDaysValue();
            }
        });
        weekly_fri.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    fri = ", Fri";
                } else {
                    fri = "";
                }
                updateWeeklyDays();
                setRepeatDaysValue();
            }
        });
        weekly_sat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    sat = ", Sat";
                } else {
                    sat = "";
                }
                updateWeeklyDays();
                setRepeatDaysValue();
            }
        });
        weekly_sun.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    sun = ", Sun";
                } else {
                    sun = "";
                }
                updateWeeklyDays();
                setRepeatDaysValue();
            }
        });

    }

    private void getSelectedRepeatOption() {
        String temp_repeat = db.getSelectedScheduleRepeatDays(sched_id);
        if (repeated) {
            switch (temp_repeat) {
                case "Everyday":
                    setCheckedRepeatOption(everyday);
                    break;
                case "Weekdays":
                    setCheckedRepeatOption(weekdays);
                    break;
                case "Weekends":
                    setCheckedRepeatOption(weekend);
                    break;
                default:
                    setCheckedRepeatOption(weekly);
                    weekly_days_container.setVisibility(View.VISIBLE);
                    if (db.getSelectedScheduleRepeatDays(sched_id).contains("Mon")) {
                        weekly_mon.setChecked(true);
                    }
                    if (db.getSelectedScheduleRepeatDays(sched_id).contains("Tue")) {
                        weekly_tue.setChecked(true);
                    }
                    if (db.getSelectedScheduleRepeatDays(sched_id).contains("Wed")) {
                        weekly_wed.setChecked(true);
                    }
                    if (db.getSelectedScheduleRepeatDays(sched_id).contains("Thu")) {
                        weekly_thu.setChecked(true);
                    }
                    if (db.getSelectedScheduleRepeatDays(sched_id).contains("Fri")) {
                        weekly_fri.setChecked(true);
                    }
                    if (db.getSelectedScheduleRepeatDays(sched_id).contains("Sat")) {
                        weekly_sat.setChecked(true);
                    }
                    if (db.getSelectedScheduleRepeatDays(sched_id).contains("Sun")) {
                        weekly_sun.setChecked(true);
                    }
                    break;
            }
            updateWeeklyDays();
        }
    }

    private void getSelectedSchedule() {
        turned_on = db.getSelectedScheduleEnableStatus(sched_id);
        sched_time = db.getSelectedScheduleTime(sched_id);
        repeated = db.getSelectedScheduleRepeatStatus(sched_id);

        String[] temp_repeat = db.getSelectedScheduleRepeatDays(sched_id).split(",");

        if (temp_repeat[0].equals("Everyday") || temp_repeat[0].equals("Weekdays") || temp_repeat[0].equals("Weekends")) {
            selected_repeat_text = temp_repeat[0];
        } else {
            if (temp_repeat.length >= 1) {
                mon = ", " + temp_repeat[0].trim();
            }
            if (temp_repeat.length >= 2) {
                tue = ", " + temp_repeat[1].trim();
            }
            if (temp_repeat.length >= 3) {
                wed = ", " + temp_repeat[2].trim();
            }
            if (temp_repeat.length >= 4) {
                thu = ", " + temp_repeat[3].trim();
            }
            if (temp_repeat.length >= 5) {
                fri = ", " + temp_repeat[4].trim();
            }
            if (temp_repeat.length >= 6) {
                sat = ", " + temp_repeat[5].trim();
            }
            if (temp_repeat.length >= 7) {
                sun = ", " + temp_repeat[6].trim();
            }
        }

        updateWeeklyDays();
    }

    private void updateTimeText() {
        String myFormat = "hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        if (calendar == null) { calendar = Calendar.getInstance(); }
        time_text_value.setText(sdf.format(calendar.getTime()));
        db.setTime(sched_id, sdf.format(calendar.getTime()));
    }

    private void setCheckedRepeatOption(RadioButton radioButton) {

        // Set All Radio Buttons Checked State to False
        everyday.setChecked(false);
        weekdays.setChecked(false);
        weekend.setChecked(false);
        weekly.setChecked(false);

        // Then check the selected Radio Button
        radioButton.setChecked(true);

        // Check if Weekly is not selected then uncheck the repeat days
        try {
            if (!weekly.isChecked()) {
                uncheckWeeklyDays();
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        // Hide Days if Weekly Radio Button is not checked
        weekly_days_container.setVisibility(View.GONE);
    }

    private void setRepeatDaysValue() {
        repeat_text_view.setText(selected_repeat_text);

        if (!selected_repeat_text.equals("")) {
            repeat_text_view.setVisibility(View.VISIBLE);
        } else {
            repeat_text_view.setVisibility(View.GONE);
        }

        db.setRepeatDays(sched_id, selected_repeat_text);
    }

    private void updateWeeklyDays() {
        selected_repeat_text = mon + tue + wed + thu + fri + sat + sun;

        if (selected_repeat_text.length() > 0) {
            if (selected_repeat_text.substring(0, 1).equals(",")) {
                selected_repeat_text = selected_repeat_text.substring(1);
            }
        } else {
            selected_repeat_text = "";
        }

        selected_repeat_text = selected_repeat_text.trim();
    }

    private void uncheckWeeklyDays() {
        weekly_mon.setChecked(false);
        weekly_tue.setChecked(false);
        weekly_wed.setChecked(false);
        weekly_thu.setChecked(false);
        weekly_fri.setChecked(false);
        weekly_sat.setChecked(false);
        weekly_sun.setChecked(false);
    }
}
